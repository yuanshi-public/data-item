## DataItem

数据的格式化

- 参数

_type_ (string) - 值类型：text - 文字和数字 (默认)，html - 包含 html 标签的文字 **非必填**

_label_ (string) - 标签 **非必填**

_value_ (string|number) - 值 **非必填**

_labelPosition_ (string) - 标签的位置： 'left' (默认), 'right', 'top', 'bottom' **非必填**

_width_ (string|number) - 总宽度, 默认为 100%; 类型为 number 的话，宽度单位为 px **非必填**

_labelWidth_ (string|number) - 标签的宽度，默认为 6.5rem; 类型为 number 的话，宽度单位为 px **非必填**

_color_ (string) - 颜色主题： 'grey' (默认), 'blue', 'red', 'green', 'yellow' **非必填**

_labelStyle_ (Object) - 标签的样式 **非必填**

_valueStyle_ (Object) - 值的样式 **非必填**

_showNoValue_ (boolean) - 是否显示无值标题

_noValue_ (string) - 无值时候显示的标题 **非必填**

_noValueStyle_ (Object) - 无值标题的样式 **非必填**

### 实例

```html
<DataItem type="html" labelWidth="6rem" label="内容:" :value="details.html" width="600" noValue="无" color="green" />

<DataItem label="容量:" :labelStyle="{ color: '#149d24' }" :value="details.volume" showNoValue noValue="无" />

<DataItem
  label="容量:"
  :valueStyle="{ backgroundColor: '#7cd786', color: '#149d24', border: '1px dashed #149d24' }"
  :value="details.volume"
  showNoValue
  noValue="无"
  :noValueStyle="{ color: 'red' }"
/>
<!-- 左边的半框有绿边线、绿背景框。值是绿色的。无值时候，‘无’是红色的 -->

<DataItem label="数字" labelPosition="top" color="red">
  <ul>
    <li>1</li>
    <li>2</li>
    <li>3</li>
  </ul>
</DataItem>

<DataItem label="无数据" showNoValue>
  <template #novalue>
    <i class="icon"></i>
  </template>
</DataItem>
<!-- 无值的时候，展示图标 -->
```
